FROM fedora:latest
RUN dnf -y install make pandoc latexmk python3-beautifulsoup4 graphviz
RUN dnf -y install texlive-{appendixnumberbeamer,babel-english,beamertheme-metropolis,paratype,parskip,polyglossia,sansmathfonts,textpos,upquote,was,xurl}

