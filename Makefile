include config.mk

INPUT = $(shell find body -type f -name "[0-9][0-9]_*.md" | sort -n)
BACKUPINPUT = $(shell find appendix -type f -name "[0-9][0-9]_*.md" | sort -n)
PAPERCITATIONS = $(patsubst %, refs/%.tex, $(shell grep -hoE "LHCb-(PAPER|DP|PUB|FIGURE)-[0-9]{4}-[0-9]{3}" $(INPUT) $(BACKUPINPUT) | sort | uniq))
PAPERPLOTS = $(patsubst %, figs/%, $(shell grep -hoE "Directory_LHCb-(PAPER|DP|PUB|FIGURE)-[0-9]{4}-[0-9]{3}/[\w\.\/\-]*" $(INPUT) $(BACKUPINPUT) | sort | uniq))
PAPERSEOS="root://eosproject.cern.ch//eos/project/l/lhcbwebsites/www/lhcbproject/Publications"
GRAPHVIZFIGS = $(patsubst graphviz/src/%.dot, graphviz/%.pdf, $(shell find graphviz/src -type f -name "*.dot"))

.PHONY: all clean tidy show

all: $(NAME).pdf
	@$(MAKE) -s --no-print-directory tidy

clean: tidy
	rm -f $(NAME).{tex,log,pdf} backup.tex

tidy:
	rm -f $(NAME).{aux,nav,out,snm,toc,xwm,vrb,fls,fdb_latexmk}

$(NAME).pdf: $(NAME).tex backup.tex $(shell find figs head texmf -type f) $(GRAPHVIZFIGS) $(PAPERPLOTS) $(PAPERCITATIONS) | figs/
	@TEXMFHOME=texmf latexmk -pdf -silent $(NAME) 1>/dev/null || (sed -e '/^!.*/,/^$$/!d' $(NAME).log 1>&2; ./scripts/parse_log.sh Error $(NAME).log 1>&2; rm $(NAME).pdf; exit 1)
	@./scripts/parse_log.sh Warning $(NAME).log 1>&2

.INTERMEDIATE:
$(NAME).tex: $(INPUT) metadata.yml head/preamble.tex
	@echo "$(word 1, $(PANDOC)): '$(INPUT)' -> '$@'"
	@$(PANDOC) $(INPUT) -t beamer --standalone --include-in-header head/preamble.tex --metadata-file metadata.yml -o $@
	@sed -i.bak 's/\\frame{\\titlepage}/\\frame[plain,noframenumbering]{\\titlepage}/' $@
	@sed -i.bak 's/ \\and/, /g' $@
	@sed -i.bak 's/:,/: /g' $@
	@rm $@.bak
	@./scripts/remove_noframes.sh $@

.INTERMEDIATE:
backup.tex: $(BACKUPINPUT)
	@echo "$(word 1, $(PANDOC)): '$^' -> '$@'"
	@$(PANDOC) $(BACKUPINPUT) -t beamer -o $@
	@./scripts/remove_noframes.sh $@

%/:
	mkdir -p $@

refs/%.tex: | refs/
	python scripts/get_reference.py $* > $@

figs/Directory_%: | figs/
	xrdcp -s $(PAPERSEOS)/LHCbProjectPublic/Directory_$* $@ || xrdcp -s $(PAPERSEOS)/p/Directory_$* $@ || xrdcp -s $(PAPERSEOS)/l/Directory_$* $@

graphviz/%.pdf: graphviz/src/%.dot | graphviz/src/
	dot -Tpdf $< > $@

show:
	$(IMPRESSIVE) $(NAME).pdf

.PHONY:
overleaf: $(NAME).tex backup.tex texmf/tex/latex/*.sty
	mkdir -p overleaf
	cp -r figs head logos $^ overleaf


