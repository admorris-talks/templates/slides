# name of output .pdf file
NAME = main

# talk duration if using `impressive` to show the slides
LENGTH = 20:00

# LaTeX command
#LATEX = pdflatex -interaction=batchmode -halt-on-error
LATEX = latexmk -pdf -silent

# pandoc command
SOURCE_FORMAT = "markdown+pipe_tables+backtick_code_blocks+auto_identifiers+strikeout+yaml_metadata_block+implicit_figures+link_attributes+smart+fenced_divs+all_symbols_escapable"
PANDOC = pandoc --slide-level 2 --listings -f $(SOURCE_FORMAT)

# impressive command
IMPRESSIVE = impressive --fake-fullscreen --geometry 1600x900 --noclick --transition None --duration $(LENGTH) --minutes --mousedelay 500 --layout time=BL

