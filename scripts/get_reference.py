#!/usr/bin/env python

from bs4 import BeautifulSoup
import requests
import sys
import time

paper = sys.argv[1]

url = f"https://lhcbproject.web.cern.ch/Publications/LHCbProjectPublic/{paper}.html"

# EOSWeb is unreliable so retry 5 times
for i in range(5):
	r = requests.get(url)
	if r.ok:
		break
	time.sleep(i)
else:
	r.raise_for_status()

soup = BeautifulSoup(r.content, "html.parser")
info = soup.body.find_all("div", class_="span2")[0]
items = info.ul.find_all("li")

def get_matching_item(items, pattern):
	matching_items = list(filter(lambda i: pattern in str(i), items))
	if len(matching_items) == 1:
		return matching_items[0]
	else:
		raise ValueError(f"Wrong number of items matching \"{pattern}\": {matching_items}")

def submitted(items):
	try:
		get_matching_item(items, "arxiv")
		return True
	except:
		return False

def get_arxiv(items):
	arxiv = get_matching_item(items, "arxiv")
	return arxiv.h3.a.text.strip()

def get_doi(items):
	doi = get_matching_item(items, "doi")
	return "/".join(doi.h3.a["href"].split("/")[3:])

def get_ref(items):
	return get_matching_item(items, "doi").text.strip()

def make_citation(items):
	if published(items):
		doi = get_doi(items)
		ref = get_ref(items)
		return f"\\paperdoi{{{doi}}}{{{ref}}}"
	elif submitted(items):
		arxiv_num = get_arxiv(items).split(":")[-1]
		return f"\\arxiv{{{arxiv_num}}}"
	elif "PAPER" in paper:
		return f"\\lbpaper{{{paper}}}, in preparation"
	else:
		return f"\\lbpaper{{{paper}}}"

def published(items):
	try:
		doi = get_doi(items)
		if doi == "":
			return False
		return True
	except:
		return False

print(make_citation(items))
