#!/bin/bash
set -uo pipefail

PATTERN=$1
FILE=$2

grep -a "Fatal" $FILE
grep -a "! LaTeX $PATTERN" $FILE

PACKAGES=$(grep -aoE "Package ([A-Za-z0-9_]*) $PATTERN" $FILE | awk '{print $2}' | uniq)

for PACKAGE in $PACKAGES
do
	grep $PACKAGE $FILE
done

