#!/bin/bash
set -uo pipefail

SOURCE=$1

perl -i -pe 's/\\begin{frame}{NOFRAME}.*?\\end{frame}//gsm' "${SOURCE}"
